package pages;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import webdriver.Utils;

public class ProductPage extends PageObject {
    String btnAddtoCartSauceLabsBackpackXPath = "//button[@id='add-to-cart-sauce-labs-backpack']";
    String btnShoppingCartXPath = "//a[@data-test='shopping-cart-link']";
    String txtProductXPath = "//span[text()='Products']";
    String txtSauceLabsBackpackXPath ="//div[text()='Sauce Labs Backpack']";
    String txtSauceLabsBikeLightXPath ="//div[text()='Sauce Labs Bike Light']";
    String btnBurgerMenuXPath = "//button[@id='react-burger-menu-btn']";

    public void clickAddtoCartSaucelabsBackpack() {
        WebElement btnAddtoCartSauceLabsBackpack = Utils.device.findElement(By.xpath(btnAddtoCartSauceLabsBackpackXPath));
        waitABit(2000);
        btnAddtoCartSauceLabsBackpack.click();

    }

    public void clickShoppingCart() {
        WebElement btnShoppingCart = Utils.device.findElement(By.xpath(btnShoppingCartXPath));
        waitABit(2000);
        btnShoppingCart.click();

    }

    public boolean verifyProductPage() {
        waitABit(2000);
        try {
            Utils.device.findElement(By.xpath(txtProductXPath));
            Utils.device.findElement(By.xpath(txtSauceLabsBackpackXPath));
            Utils.device.findElement(By.xpath(txtSauceLabsBikeLightXPath));
            Utils.device.findElement(By.xpath(btnBurgerMenuXPath));
            return true;

        } catch (Exception e) {
            return false;
        }
    }
}

package pages;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.AWTException;
import webdriver.Utils;

import java.net.MalformedURLException;

public class CommonPage extends PageObject {

    public static void pleaseWaitFor(int i) {
        try {
            Thread.sleep(i);
        } catch (Exception e) {
            // Log or handle any exceptions
        }
    }

    public void openWebBrowser() throws MalformedURLException {
        Utils.device = Utils.newWebDriver();
        pleaseWaitFor(2000);
        Utils.device.get("https://www.saucedemo.com/");
        pleaseWaitFor(5000);
    }

    public void userClick(String text) {

        WebElement webElement = Utils.device.findElement(By.xpath("//input[@value='"+text+"']" + "|//button[text()='"+text+"']" + "|//a[text()='"+text+"']"));


        //waitFor(webElement);
        waitABit(2000);
        webElement.click();
        waitABit(2000);
    }

    public void userClickProduct(String text) {

        WebElement webElement = Utils.device.findElement(By.xpath("//div[text()='"+text+"']" ));

        //waitFor(webElement);
        waitABit(2000);
        webElement.click();
        waitABit(2000);
    }





    public boolean verifyTextIsVisible(String text) {
        waitABit(2000);
        try {
            Utils.device.findElement(By.xpath("//*[contains(.,'"+text+"')]"));
            return true;

        } catch (Exception e) {
            return false;
        }
    }

}

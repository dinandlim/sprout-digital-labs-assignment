package pages;

import io.appium.java_client.MobileElement;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import webdriver.Utils;

public class CheckoutPage extends PageObject {

    String txtFirstNameXPath = "//input[@data-test='firstName']";
    String txtPostalCodeXPath = "//input[@data-test='postalCode']";
    String txtLastNameXPath = "//input[@data-test='lastName']";
    String txtTotalPriceXPath = "//div[@data-test='total-label' and text()='Total: $' and text()='43.18']";

    public void inputFirstName(String text) {
        WebElement txtFirstName = Utils.device.findElement(By.xpath(txtFirstNameXPath));
        waitABit(2000);
        txtFirstName.clear();
        txtFirstName.sendKeys(text);
    }

    public void inputLastName(String text) {
        WebElement txtLastName = Utils.device.findElement(By.xpath(txtLastNameXPath));
        waitABit(2000);
        txtLastName.clear();
        txtLastName.sendKeys(text);
    }

    public void inputPostalCode(String text) {
        WebElement txtPostalCode = Utils.device.findElement(By.xpath(txtPostalCodeXPath));
        waitABit(2000);
        txtPostalCode.clear();
        txtPostalCode.sendKeys(text);
    }

    public boolean verifyTotalPrice() {
        waitABit(2000);
        try {
            Utils.device.findElement(By.xpath(txtTotalPriceXPath));
            return true;

        } catch (Exception e) {
            return false;
        }
    }



}

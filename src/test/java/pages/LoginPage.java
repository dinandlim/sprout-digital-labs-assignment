package pages;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import webdriver.Utils;

public class LoginPage extends PageObject {

    String txtUsernameXPath = "//input[@data-test='username']";
    String txtPasswordXPath = "//input[@data-test='password']";
    String btnLoginXPath = "//input[@id='login-button']";

    public void inputUsername(String text) {
        WebElement txtfieldUsername = Utils.device.findElement(By.xpath(txtUsernameXPath));
        waitABit(2000);
        txtfieldUsername.clear();
        txtfieldUsername.sendKeys(text);
    }

    public void inputPassword(String text) {
        WebElement txtfieldPassword = Utils.device.findElement(By.xpath(txtPasswordXPath));
        waitABit(2000);
        txtfieldPassword.clear();
        txtfieldPassword.sendKeys(text);
    }

    public boolean verifyLoginPage() {
        waitABit(2000);
        try {
            Utils.device.findElement(By.xpath(txtUsernameXPath));
            Utils.device.findElement(By.xpath(txtPasswordXPath));
            Utils.device.findElement(By.xpath(btnLoginXPath));
            return true;

        } catch (Exception e) {
            return false;
        }

    }


}

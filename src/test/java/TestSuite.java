
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

    @RunWith(CucumberWithSerenity.class)
    @CucumberOptions(
            features = "src/test/resources/features/Sprout",
            plugin = {"json:target/destination/cucumber1.json", "rerun:target/destination/rerun.txt"}
    )
    public class TestSuite {

    }


package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.CommonPage;

import java.awt.*;

import static org.junit.Assert.assertTrue;

public class CommonSteps {

    CommonPage commonPage;

    @Given("^User open saucedemo web browser$")
    public void userOpenSauceDemo() {
        try {
            commonPage.openWebBrowser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @And("^User click '(.*)' button$")
    public void userClick(String text) {
        commonPage.userClick(text);
    }

    @And("^User open product '(.*)'$")
    public void userClickProduct(String text) {
        commonPage.userClickProduct(text);
    }

    @And("^User verify and see text '(.*)'$")
    public void userVerifyAndSeeText(String text) {
        boolean isVisible = commonPage.verifyTextIsVisible(text);
        Assert.assertTrue("text is not showing", isVisible);
    }


}

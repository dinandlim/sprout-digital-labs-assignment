package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.LoginPage;


public class LoginSteps {

    LoginPage loginPage;


    @When("^User input '(.*)' in username textfield$")
    public void userInputUsername(String text) {
        loginPage.inputUsername(text);
    }

    @When("^User input '(.*)' in password textfield$")
    public void userInputPassword(String text) {
        loginPage.inputPassword(text);
    }

    @When("^Verify user is on login page$")
    public void verifyLoginPage() {
        boolean isVisible = loginPage.verifyLoginPage();
        Assert.assertTrue("User is not on login page", isVisible);
    }
}
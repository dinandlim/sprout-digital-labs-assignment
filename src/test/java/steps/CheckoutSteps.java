package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.CheckoutPage;
import pages.LoginPage;

public class CheckoutSteps {

    CheckoutPage CheckoutPage;


    @When("^User input '(.*)' in Fist Name textfield$")
    public void userInputUsername(String text) {
        CheckoutPage.inputFirstName(text);
    }

    @And("^User input '(.*)' in Last Name textfield$")
    public void userInputLastName(String text) {
        CheckoutPage.inputLastName(text);
    }

    @And("^User input '(.*)' in Postal Code textfield$")
    public void userInputPostalCode(String text) {
        CheckoutPage.inputPostalCode(text);
    }

    @And("^User verify total price$")
    public void userVerifyAndSeeText() {
        boolean isVisible = CheckoutPage.verifyTotalPrice();
        Assert.assertTrue("Total Price is wrong", isVisible);
    }
}

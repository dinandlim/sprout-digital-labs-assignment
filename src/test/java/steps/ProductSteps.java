package steps;

import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.ProductPage;

public class ProductSteps {

    ProductPage productPage;

    @When("^User click add to cart Sauce Labs Backpack$")
    public void userClickAddtoCartSauceLabsBackpack() {
        productPage.clickAddtoCartSaucelabsBackpack();
    }

    @When("^User click shopping cart button$")
    public void userClickShoppingCartBtn() {
        productPage.clickShoppingCart();
    }

    @When("^Verify user is on Home Screen page$")
    public void verifyLoginPage() {
        boolean isVisible = productPage.verifyProductPage();
        Assert.assertTrue("User is not on Home Screen page", isVisible);
    }
}

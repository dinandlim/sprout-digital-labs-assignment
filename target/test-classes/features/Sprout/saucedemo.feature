Feature: saucedemo

  Scenario: Verify saucedemo product and checkout
    Given User open saucedemo web browser
    When User input 'locked_out_user' in username textfield
    And User input 'secret_sauce' in password textfield
    And User click 'Login' button
    Then User verify and see text 'Sorry, this user has been locked out.'
    When User input 'standard_user' in username textfield
    And User input 'secret_sauce' in password textfield
    And User click 'Login' button
    And User click add to cart Sauce Labs Backpack
    And User open product 'Sauce Labs Bike Light'
    Then User click 'Add to cart' button
    When User click shopping cart button
    Then User click 'Checkout' button
    When User input 'dummy' in Fist Name textfield
    And User input '28654' in Postal Code textfield
    And User click 'Continue' button
    Then User verify and see text 'Error: Last Name is required'
    When User input 'user' in Last Name textfield
    And User click 'Continue' button
    Then User verify total price
    And User verify and see text 'Sauce Labs Backpack'
    And User verify and see text 'Sauce Labs Bike Light'
    When User click 'Finish' button
    And User click 'Back Home' button
    Then Verify user is on Home Screen page
    When User click 'Open Menu' button
    And User click 'Logout' button
    Then Verify user is on login page

    
